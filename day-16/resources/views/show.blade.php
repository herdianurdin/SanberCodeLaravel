@extends('adminlte.master')
@section('content')
<div class="card">
    <div class="card-header bg-primary">
        <h1 class="card-title text-bold">Cast</h1>
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama Pemain</label>
            <input type="text" class="form-control" id="nama" name="nama" readonly value="{{$cast->nama}}" >
        </div>
        <div class="form-group">
            <label for="umur">Umur Pemain</label>
            <input type="text" class="form-control" id="umur" name="umur" readonly value="{{$cast->umur}}" >
            </div>
        <div class="form-group">
            <label for="bio">Bio Pemain</label>
            <textarea class="form-control" id="bio" name="bio" rows="3" readonly>{{$cast->bio}}</textarea>
        </div>
        <div class="my-1">
            <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning text-bold mr-3">Edit</a>
            <form action="/cast/{{$cast->id}}" method="POST" class="d-inline">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger text-bold" value="Hapus">
            </form>
        </div>
    </div>
</div>
@endsection
