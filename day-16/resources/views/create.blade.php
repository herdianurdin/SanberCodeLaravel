@extends('adminlte.master')
@section('content')
<div class="card">
    <div class="card-header bg-primary">
        <h1 class="card-title text-bold">Cast</h1>
    </div>
    <div class="card-body">
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
              <label for="nama">Nama Pemain</label>
              <input type="text" class="form-control" id="nama" name="nama" required >
            </div>
            <div class="form-group">
                <label for="umur">Umur Pemain</label>
                <input type="number" min="7" max="150" class="form-control" id="umur" name="umur" required >
              </div>
            <div class="form-group">
              <label for="bio">Bio Pemain</label>
              <textarea class="form-control" id="bio" name="bio" rows="3" required></textarea>
            </div>
            <div class="my-1">
                <button type="submit" class="btn btn-primary text-bold">Simpan</button>
              </div>
          </form>
    </div>
</div>
@endsection