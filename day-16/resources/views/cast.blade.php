@extends('adminlte.master')
@section('content')
<div class="card">
    <div class="card-header bg-primary">
        <h1 class="card-title text-bold">Cast</h1>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <a href="/cast/create" class="btn btn-primary text-bold" role="button">Tambah Data</a>
        </div>
        <div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center" style="width: 10px">Umur</th>
                        <th class="text-center">Bio</th>
                        <th class="text-center" style="width: 10px">View</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=>$value)
                    <tr>
                        <td class="text-center">{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        <td class="text-center">{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td class="text-center">
                            <a href="/cast/{{$value->id}}" class="btn btn-dark" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="5">No data...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
