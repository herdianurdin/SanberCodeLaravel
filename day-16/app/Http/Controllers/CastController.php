<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    function index() {
        $cast = DB::table('cast')->get();

        return view('cast', compact('cast'));
    }

    function create() {
        return view('create');
    }

    function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('show', compact('cast'));
    }

    function edit($id) {
        $cast = DB::table('cast')
            ->where('id', $id)
            ->first();

        return view('edit', compact('cast'));
    }

    function update($id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);

        return redirect('/cast');
    }

    function destroy($id) {
        $query = DB::table('cast')
            ->where('id', $id)
            ->delete();

        return redirect('/cast');
    }
}
