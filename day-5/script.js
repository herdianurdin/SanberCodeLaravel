const items = [
    ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'],
    ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
    ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
    ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg'],
]

let count = 0

const listBarang = document.querySelector('#listBarang')
const formItem = document.querySelector('#formItem')
const searchInput = document.querySelector('#keyword')

const cardView = (item) => {
    return `
            <div class="card" style="width: 18rem;">
                <img src="img/${item[4]}" class="card-img-top" height="200px" width="200px" alt="${item[1]}">
                <div class="card-body">
                    <h5 class="card-title" id="itemName">${item[1]}</h5>
                    <p class="card-text" id="itemDesc">${item[3]}</p>
                    <p class="card-text">Rp ${item[2].toFixed(0).replace(/(\d)(?=(\d{3})+\b)/g, '$1.')}</p>
                    <a href="#" class="btn btn-primary" id="addCart" onClick="add()">Tambahkan ke keranjang</a>
                </div>
            </div>
    `
}

const add = () => {
    count++
    document.querySelector('#count').innerHTML = count
}

items.forEach((item) => {
    listBarang.innerHTML += cardView(item)
})

formItem.addEventListener('submit', () => {
    listBarang.innerHTML = ''

    items.forEach((item) => {
        if (
            item[1].toLowerCase().includes(searchInput.value.toLowerCase()) ||
            item[3].toLowerCase().includes(searchInput.value.toLowerCase())
        )
            listBarang.innerHTML += cardView(item)
    })
})
