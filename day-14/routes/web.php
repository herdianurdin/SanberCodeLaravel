<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = ['title' => 'Home'];

    return view('index', $data);
});

Route::get('/tables', function() {
    $data = ['title' => 'Tables'];

    return view('tables', $data);
});

Route::get('/data-tables', function() {
    $data = ['title' => 'DataTables'];

    return view('data-tables', $data);
});