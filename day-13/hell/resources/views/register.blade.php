@extends('master')

@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST" >
        @csrf
        <p><label for="first-name">First name:</label></p>
        <input type="text" id="first-name" name="first" />
        <p><label for="last-name">Last name:</label></p>
        <input type="text" id="last-name" name="last" />
        <p><label for="gender">Gender:</label></p>
        <input type="radio" name="gender" value="0" id="gender" /> Male <br />
        <input type="radio" name="gender" value="1" id="gender" /> Female <br />
        <input type="radio" name="gender" value="2" id="gender" /> Other <br />

        <p><label for="national">Nationality:</label></p>
        <select id="national">
            <option value="0">Indonesian</option>
            <option value="1">Malaysia</option>
            <option value="2">Singapore</option>
        </select>
        <p><label>Language Spoken:</label></p>
        <input type="checkbox" name="language" value="0" /> Bahasa Indonesian <br />
        <input type="checkbox" name="language" value="1" /> English <br />
        <input type="checkbox" name="language" value="2" /> Other
        <p><label>Bio:</label></p>
        <div>
            <textarea cols="30" rows="10"></textarea>
        </div>
        <input type="submit" value="Sign Up" />
    </form>
@endsection