<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        $data = ['title' => 'Form Daftar Account'];

        return view('register', $data);
    }

    public function welcome(Request $request) {
        $data = [
            'title' => 'Selamat Datang!',
            'first' => $request['first'],
            'last' => $request['last']
        ];

        return view('welcome', $data);
    }
}
