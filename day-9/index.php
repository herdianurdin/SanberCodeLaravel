<?php
    require_once "animal.php";
    require_once "frog.php";
    require_once "ape.php";

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name\n";
    echo "legs : $sheep->legs\n";
    echo "cold : $sheep->cold_blooded\n\n";

    $kodok = new Frog("buduk");
    echo "Name : $kodok->name\n";
    echo "legs : $kodok->legs\n";
    echo "cold : $kodok->cold_blooded\n";
    echo "Jump : ";
    $kodok->jump();

    $sungokong = new Ape("kera sakti");
    echo "Name : $sungokong->name\n";
    echo "legs : $sungokong->legs\n";
    echo "cold : $sungokong->cold_blooded\n";
    echo "Yell : ";
    $sungokong->yell();

